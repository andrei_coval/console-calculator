package ro.consolecalculator;

import java.util.*;

public class App {

    InputManager inputManager;
    StringProcessor stringProcessor;

    public App() {
        inputManager = new InputManager();
        stringProcessor = new StringProcessor();
    }

    public static void main(String[] args) {
        App app = new App();
        app.initCalculator();
	// write your code here
    }

    public static String VERSION = "1.0";

    public void initCalculator()
    {
        Scanner keyboard = new Scanner(System.in);
        System.out.println("enter the number1");
        float myint1 = keyboard.nextInt();
        System.out.println("enter the number2");
        float myint2 = keyboard.nextInt();
        System.out.println("enter the operation");
        String operation = keyboard.next();

        float res = inputManager.compute(myint1,myint2,operation);
        System.out.println("the result is:" + res);
    }

    public void initStringProcessor()
    {
        Scanner keyboard = new Scanner(System.in);
        System.out.println("enter the string");
        String string = keyboard.next();

        System.out.println("enter the operation");
        String operation = keyboard.next();

        float res = stringProcessor.process(string,operation);
        System.out.println("the result is:" + res);
    }
}
