package ro.consolecalculator;

/**
 * Created by coval on 6/6/2015.
 */
public class Calculator
{
    public float sum(final float a, final float b)
    {
        float restultat = a + b;
        return restultat;
    }

    public float subtract(final float c, final float d)
    {
        float resultat = c - d;
        return resultat;
    }

    public float multiply(final float e, final float f)
    {
        float resultat = e * f;
        return resultat;
    }

    public float divide(final float g, final float h)
    {
        return g / h;
    }
}
