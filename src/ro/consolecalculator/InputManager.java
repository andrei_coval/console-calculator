package ro.consolecalculator;

/**
 * Created by coval on 6/6/2015.
 */
public class InputManager {

    private Calculator calculator;

    public InputManager() {
        calculator = new Calculator();
    }

    public float compute(float val1, float val2, String operation)
    {
        float result = 0;

        switch (operation)
        {
            case "+":
                result = calculator.sum(val1, val2);
                break;
            case "-":
                result = calculator.subtract(val1,val2);
                break;
            case "*":
                result = calculator.multiply(val1,val2);
                break;
            case "/":
                result = calculator.divide(val1,val2);
                break;
        }

        return result;
    }
}
